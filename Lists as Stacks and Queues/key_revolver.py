from collections import deque


def where_to_start(petrol_pumps):
    petrol_pumps = deque(petrol_pumps)
    petrol_in_truck = 0
    current = 0
    counter = 0

    while len(petrol_pumps)-1 > 1:
        pump = petrol_pumps.popleft().split()
        petrol_in_truck += (int(pump[0]) - int(pump[-1]))
        if petrol_in_truck < 0:
            current = counter + 1
            petrol_in_truck = 0
        counter += 1
    return current


#tests = [
#    [
#        '4',
#        [
#            '30 2',
#            '1 5',
#            '3 11',
#            '10 3',
#        ]
#    ],
#]
#
#[print(where_to_start(petrol_pumps)) for [n, petrol_pumps] in tests]


petrol_pumps = []
n = int(input())
for i in range(n):
    petrol_pumps.append(input())


print(where_to_start(petrol_pumps))

